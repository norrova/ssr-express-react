import React from 'react';

export default function App() {
    return (
      <html>
        <head>
          <meta charSet="utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <link rel="stylesheet" href="/styles.css"></link>
          <title>My app</title>
        </head>
        <body>
          <p>Powered by Express with SSR React</p>
        </body>
      </html>
    );
  }
  