import express from 'express';
import cors from 'cors';
import ReactDOMServer from "react-dom/server";
import React from 'react';
import App from './src/App';

const app = express();

app.use(cors());

app.get('/', (req, res) => {
  const { pipe } = ReactDOMServer.renderToPipeableStream(<App />, {
    bootstrapScripts: ['/bootstrap.js'],
    onShellReady() {
      res.setHeader('content-type', 'text/html');
      pipe(res);
    }
  });
});

app.listen(8080, () =>
  console.log('App ready on port 8080!'),
);