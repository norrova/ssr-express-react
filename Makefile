DOCKER-COMPOSE = docker-compose
APP = app

boot: # Booting the application
	$(DOCKER-COMPOSE) up -d
connect:
	$(DOCKER-COMPOSE) exec -it $(APP) sh
logs:
	$(DOCKER-COMPOSE) logs $(APP) -f --tail 10